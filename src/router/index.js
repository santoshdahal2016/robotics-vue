import Vue from 'vue'
import Login from '../views/authentication/Login.vue'
import Home from '../views/home.vue'
import Event from '../views/event.vue'

import Dashboard from '../views/dashboard.vue'
import Layout from '../views/layout/Layout'
import Table from '../views/user/index.vue'
import Register from '../views/authentication/Register.vue'
import Verify from '../views/authentication/Verify.vue'
import Router from 'vue-router'


import App from '../views/app/index.vue'
import Slide from '../views/app/slide.vue'
import Menu from '../views/app/menu.vue'
import Program from '../views/event/index.vue'
import ProgramCreate from '../views/event/create.vue'
import ProgramEdit from '../views/event/edit.vue'


import Response from '../views/response/index.vue'
import ResponseDetail from '../views/response/detail.vue'

import Document from '../views/document/index.vue'
import DocumentCreate from '../views/document/create.vue'
import DocumentEdit from '../views/document/edit.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: "/login",
            component: Login,
            meta: {
                forVisitors: true,
                nav: false,
                roles: ['admin', 'customer']

            }
            , hidden: true
        },
        {
            path: "/register",
            component: Register,
            meta: {
                forVisitors: true,
                nav: false,
                roles: ['admin', 'customer']

            }
            , hidden: true
        },
        {
            path: "/verify",
            component: Verify,
            meta: {
                forVisitors: true,
                nav: false,
                roles: ['admin', 'customer']

            }
            , hidden: true
        },
        {
            path: '/dashboard',
            component: Layout,
            redirect: '/dashboard/index',
            meta: {
                roles: ['admin', 'customer']
            },
            children: [{
                path: 'index',
                component: Dashboard,
                name: 'dashboard',

                meta: {
                    title: 'dashboard',
                    icon: 'dashboard',
                    noCache: true,
                    forAuth: true,
                    nav: false,
                    roles: ['admin', 'customer']
                }
            }]
        },
        {
            path: '/user',
            component: Layout,
            redirect: '/user/list',
            name: 'User',
            meta: {
                title: 'User',
                icon: 'user',
                forAuth: true,
                nav: false,
                roles: ['admin']
            },
            children: [
                {
                    path: 'list',
                    name: 'List',
                    component: Table,
                    meta: {
                        title: 'User',
                        icon: 'user',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                }

            ]
        },
        {
            path: '/program',
            component: Layout,
            redirect: '/program/program',
            name: 'Event',
            meta: {
                title: 'Program',
                icon: 'guide',
                forAuth: true,
                nav: false,
                roles: ['admin']
            },
            children: [
                {
                    path: 'program',
                    name: 'Event',
                    component: Program,
                    meta: {
                        title: 'Event',
                        icon: 'guide',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                },
                {
                    path: 'create',
                    name: 'Create',
                    component: ProgramCreate,
                    meta: {
                        title: 'Event Create',
                        icon: 'edit',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                }
                ,
                {
                    path: 'edit/:slug',
                    name: 'Edit',
                    component: ProgramEdit,
                    meta: {
                        title: 'Event Edit',
                        icon: 'table',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                    , hidden: true

                }

            ]
        },
        {
            path: '/document',
            component: Layout,
            redirect: '/document/document',
            name: 'Document',
            meta: {
                title: 'Document',
                icon: 'documentation',
                forAuth: true,
                nav: false,
                roles: ['admin']
            },
            children: [
                {
                    path: 'document',
                    name: 'Document',
                    component: Document,
                    meta: {
                        title: 'Document',
                        icon: 'documentation',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                },
                {
                    path: 'create',
                    name: 'Document Create',
                    component: DocumentCreate,
                    meta: {
                        title: 'Document Create',
                        icon: 'edit',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                }
                ,
                {
                    path: 'edit/:id',
                    name: 'Document Edit',
                    component: DocumentEdit,
                    meta: {
                        title: 'Document Edit',
                        icon: 'edit',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                    , hidden: true
                }


            ]
        },
        {
            path: '/response',
            component: Layout,
            redirect: '/response/response',
            name: 'Response',
            meta: {
                title: 'Response',
                icon: 'excel',
                forAuth: true,
                nav: false,
                roles: ['admin']
            },
            children: [
                {
                    path: 'reponse',
                    name: 'Response',
                    component: Response,
                    meta: {
                        title: 'Response',
                        icon: 'excel',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                },
                {
                    path: 'responsedetail/:id',
                    name: 'Detail',
                    component: ResponseDetail,
                    meta: {
                        title: 'Response Detail',
                        icon: 'table',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                    , hidden: true
                }


            ]
        },
        {
            path: "/",
            component: Home,
            meta: {
                forVisitors: true,
                nav: true,
                roles: ['admin', 'customer']

            }
            , hidden: true
        },
        {
            path: "/event/:slug",
            component: Event,
            meta: {
                forVisitors: true,
                nav: false,
                roles: ['admin', 'customer']

            }
            , hidden: true
        },
        {
            path: '/app',
            component: Layout,
            redirect: '/app/app',
            name: 'App',
            meta: {
                title: 'App',
                icon: 'example',
                forAuth: true,
                nav: false,
                roles: ['admin']
            },
            children: [
                {
                    path: 'app',
                    name: 'app',
                    component: App,
                    meta: {
                        title: 'App',
                        icon: 'example',
                        forAuth: true,
                        nav: false,
                        roles: ['admin']
                    }
                },
                {
                    path: 'slide',
                    component: Slide,
                    name: 'Slide',
                    meta: {
                        title: 'Slide',
                        noCache: true,
                        nav: false,
                        roles: ['admin']
                    },
                    hidden: true,
                }
                ,
                {
                    path: 'menu',
                    component: Menu,
                    name: 'Menu',
                    meta: {
                        title: 'Menu',
                        noCache: true,
                        nav: false,
                        roles: ['admin']
                    },
                    hidden: true,
                },


            ]
        }


    ]
})

export default router